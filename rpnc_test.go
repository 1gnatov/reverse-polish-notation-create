package main

import (
	"reflect"
	"testing"
)

func TestSimple(t *testing.T) {
	result := Parse([]string{"-10", "+", "3", "*", "6", "/", "7", "-", "1"})
	expect := []string{"-10", "3", "6", "*", "7", "/", "+", "1", "-"}
	if !reflect.DeepEqual(result, expect) {
		panic("")
	}
}

func TestComplex(t *testing.T) {
	result := Parse([]string{"x", "+", "10", "+", "(", "2", "-", "x", ")", "*", "(", "3", "+", "(", "1", "/", "x", ")", "*", "sin", "(", "2", "*", "x", ")", ")"})
	expect := []string{"x", "10", "+", "2", "x", "-", "3", "1", "x", "/", "2", "x", "*", "sin", "*", "+", "*", "+"}
	if !reflect.DeepEqual(result, expect) {
		panic("")
	}
}
