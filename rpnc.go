package main

import (
	"fmt"
	"strconv"
	"strings"
)

func popLastElement(slice *[]string) string {
	slc := *slice
	lastElem := slc[len(slc)-1]
	slc = slc[:len(slc)-1]
	*slice = slc
	return lastElem
}

func appendLastElement(slice *[]string, elem string) {
	slc := *slice
	slc = append(slc, elem)
	*slice = slc
}

func getPriority(operation string) int {
	switch operation {
	case "sin":
		return 3
	case "*", "/":
		return 2
	case "+", "-":
		return 1
	default:
		return 0
	}
}

// Return true if a is more prioritized operation than b.
func prioritizedOperation(a, b string) bool {
	return getPriority(a) > getPriority(b)
}


// Parse function in Revese-Polish notated format
func Parse(input []string) []string {
	rpn := make([]string, 0)[:]
	buffer := make([]string, 0)[:]
	for _, elem := range input {
		if _, err := strconv.ParseFloat(elem, 32); err == nil {
			appendLastElement(&rpn, elem)
		} else if strings.ToLower(elem) == "x" {
			appendLastElement(&rpn, elem)
		} else if elem == "(" {
			appendLastElement(&buffer, elem)
		} else if elem == ")" {
			done := false
			for !done {
				bufferElem := popLastElement(&buffer)
				if bufferElem == "(" {
					done = true
				} else {
					appendLastElement(&rpn, bufferElem)
				}
			}

		} else {
			processOperations(&buffer, elem, &rpn)
		}
	}
	for len(buffer) != 0 {
		appendLastElement(&rpn, popLastElement(&buffer))
	}
	return rpn
}

func main() {
	function := []string{"-10", "+", "3", "*", "6", "/", "7", "-", "1"}
	result := Parse(function)
	fmt.Printf("%v", result)
}

func processOperations(buffer *[]string, elem string, rpn *[]string) {
	if len(*buffer) == 0 || (len(*buffer) == 1 && (*buffer)[0] == "(") {
			appendLastElement(buffer, elem)
		} else if lastOp := (*buffer)[len(*buffer)-1]; prioritizedOperation(elem, lastOp) {
			appendLastElement(buffer, elem)
		} else {
			lastOp := popLastElement(buffer)
			appendLastElement(rpn, lastOp)
			processOperations(buffer, elem, rpn) //recursion
		}
	}
	